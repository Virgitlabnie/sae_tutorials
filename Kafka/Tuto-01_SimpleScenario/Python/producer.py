from time import sleep
from json import dumps
from kafka import KafkaProducer

producerProperties = { "bootstrap_servers":['localhost:9092'] } 

producer = KafkaProducer(**producerProperties)

i = 0
while True:
   message = {"number":i}
   messageJson = dumps(message)
   messageBytes = messageJson.encode('utf-8')
   producer.send("test", value=messageBytes)
   i+=1
   #sleep(1)

