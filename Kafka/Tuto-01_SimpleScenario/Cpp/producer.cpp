// Compile with:
// g++ -o producer producer.cpp `pkg-config --libs --cflags cppkafka`

// Basic producer sending numbers on the "test" topic
// The output message is a JSON
// { "number": xxx }

#include <iostream>
#include <ostream>
#include <string>
#include <cppkafka/cppkafka.h>

int main() {
	// Create the config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", "localhost:9092"}
	};

	// Create the producer
	cppkafka::Producer producer(config);

	// Send messages
	unsigned int i {0};
	cppkafka::MessageBuilder builder("test");
	while(true) {
		std::ostringstream ostr;
		ostr << "{ \"number\": " << i++ << "}";
		std::string message {ostr.str()};
		builder.payload(message);
		try {
			producer.produce(builder);
		}
		catch (const cppkafka::HandleException& e) {
			std::ostringstream ostr2;
			ostr2 << e.what();
			std::string error {ostr2.str()};
			if (error.compare("Queue full") != 0) { 
				std::chrono::milliseconds timeout(3000);	
				producer.flush(timeout);
				producer.produce(builder);
			} else {
				std::cout << "something went wrong: " << e.what() << std::endl;
			}
		}
//		std::chrono::milliseconds timespan(1000);
//		std::this_thread::sleep_for(timespan);
	}
}

